<?php include 'koneksi.php'; ?>
<?php error_reporting(0) ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pegawai</title>
    <link rel="stylesheet" href="bootstrap.min.css" />
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="text-center">
            <h3>Data Pegawai</h3>
        </div>
        <a href="tambah.php" class="btn btn-success">Tambah Data</a>
        <?php 
            if($_GET['add'] == 'ok'){
        ?>
        <div class="mt-2 mb-2 alert alert-success alert-dismissible fade show" role="alert">
            <strong>Berhasil menambahkan data!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
            }
        ?>

        <?php 
            if($_GET['updt'] == 'ok'){
        ?>
        <div class="mt-2 mb-2 alert alert-success alert-dismissible fade show" role="alert">
            <strong>Berhasil mengubah data!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
            }
        ?>

        <?php 
            if($_GET['del'] == 'ok'){
        ?>
        <div class="mt-2 mb-2 alert alert-success alert-dismissible fade show" role="alert">
            <strong>Berhasil Menghapus data!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php
            }
        ?>
        <table class="mt-3 table table-bordered">
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>No Telepon</th>
                <th>Alamat</th>
                <th>Photo</th>
                <th>Aksi</th>
            </tr>
                <?php
                    $no = 0;
                    $sql = "SELECT * FROM daftar_pegawai dp, jabatan jb WHERE dp.id_jabatan = jb.id_jabatan";
                    $result = mysqli_query($conn,$sql);
                    while($pegawai = mysqli_fetch_assoc($result)){
                    $no++;
                ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td><?php echo $pegawai['nama']; ?></td>  
                        <td><?php echo $pegawai['jabatan'] ?></td>
                        <td><?php echo $pegawai['no_telepon'] ?></td>     
                        <td><?php echo $pegawai['alamat'] ?></td> 
                        <td>
                            <img width="100" src="images/<?php echo $pegawai['photo'] ?>"  />
                        </td>
                        <td>
                            <a href="edit.php?id=<?php echo $pegawai['id'] ?>" class="btn btn-primary btn-sm">Edit</a>
                            <a href="delete.php?id=<?php echo $pegawai['id'] ?>" class="btn btn-danger btn-sm">Delete</a>
                        </td>
                    </tr>
                <?php
                    }
                ?>
            </table>
        </div>
    </div>
</body>
</html>