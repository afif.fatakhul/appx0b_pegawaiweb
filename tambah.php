<?php include 'koneksi.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" />
    <title>Tambah</title>
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-md-6 mt-3">
            <div class="card">
                <div class="card-body">
                    <h3>Tambah Data</h3>
                    <form action="insert.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nama Pegawai</label>
                            <input name="nama" class="form-control" />    
                        </div> 
                        <div class="form-group">
                            <label>Jabatan</label>
                            <select name="jabatan" class="custom-select">
                            <?php 
                                $sql = "SELECT * FROM jabatan";
                                $result = mysqli_query($conn,$sql);
                                while($jabatan = mysqli_fetch_assoc($result)){
                            ?>
                                <option value="<?php echo $jabatan['id_jabatan'] ?>"><?php echo $jabatan['jabatan'] ?></option>
                            <?php } ?>
                            </select>    
                        </div> 
                        <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input name="no_telepon" class="form-control" />    
                        </div> 
                        <div class="form-group">
                            <label>Alamat Pegawai</label>              
                            <textarea class="form-control" name="alamat" rows="3"></textarea> 
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" class="form-control" name="photo" />
                        </div>  
                        <button type="submit" class="btn btn-primary">Simpan</button>              
                    </form>
                </div>
            </div>
        </div>
    </div>  
</body>
</html>