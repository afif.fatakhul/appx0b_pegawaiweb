<?php include 'koneksi.php'; ?>
<?php 
    $sql = "SELECT * FROM daftar_pegawai dp, jabatan jb WHERE dp.id_jabatan = jb.id_jabatan 
    AND dp.id = '$_GET[id]'";
    $result = mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap.min.css" />
    <title>Edit</title>
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-md-6 mt-3">
            <div class="card">
                <div class="card-body">
                    <h3>Edit Data</h3>
                    <form action="update.php" method="post" enctype="multipart/form-data">
                    <?php 
                     while($pegawai = mysqli_fetch_assoc($result)){
                    ?>
                        <input type="hidden" name="id_pegawai" value="<?php echo $pegawai['id'] ?>">
                        <div class="form-group">
                            <label>Nama Pegawai</label>
                            <input name="nama" class="form-control" value="<?php echo $pegawai['nama'] ?>" />    
                        </div> 
                        <div class="form-group">
                            <label>Jabatan</label>
                            <select name="jabatan" class="custom-select">
                            <?php 
                                $sql = "SELECT * FROM jabatan";
                                $result = mysqli_query($conn,$sql);
                                while($jabatan = mysqli_fetch_assoc($result)){
                                 $selected = "";
                                 if($pegawai['id_jabatan'] ==$jabatan['id_jabatab']){
                                     $selected = 'selected';
                                 }
                            ?>
                                <option <?php echo $selected; ?> value="<?php echo $jabatan['id_jabatan'] ?>"><?php echo $jabatan['jabatan'] ?></option>
                            <?php } ?>
                            </select>    
                        </div> 
                        <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input name="no_telepon" class="form-control" value="<?php echo $pegawai['no_telepon'] ?>"/>    
                        </div> 
                        <div class="form-group">
                            <label>Alamat Pegawai</label>              
                            <textarea class="form-control" name="alamat" rows="3"><?php echo $pegawai['alamat'] ?></textarea> 
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input type="file" class="form-control" name="photo" />
                            <img src="images/<?php echo $pegawai['photo'] ?>">
                        </div>  
                    <?php } ?>
                        <button type="submit" class="btn btn-primary">Simpan</button>              
                    </form>
                </div>
            </div>
        </div>
    </div>  
</body>
</html>